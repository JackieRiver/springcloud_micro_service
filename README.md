# Spring Cloud微服务示例

#### 项目介绍
学习SpringCloud非常好的演示代码,每个功能模块都拆分单独的服务演示;
包含了SpringCloud的
1.Eureka注册中心,客户端;
2.Ribbon负载均衡及自定义策略;
3.Hystrix服务降级;
4.Feign;
5.配置中心;
如有问题,https://my.oschina.net/u/3220575/blog/1920407 查看说明及源码分析,欢迎留言讨论!

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)